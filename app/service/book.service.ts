import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {RequestOptions,Headers, Request, RequestMethod} from '@angular/http';
import { Observable } from 'rxjs';
import { Book } from '../models/book';
import { Token } from '../models/user';
@Injectable()


export class BookService {
    constructor(private _http: HttpClient) { }

    private url = 'http://localhost:3000/book/';

    getAllBook(token1:string): Observable<Book> {

    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');    
    let myParams = new URLSearchParams();
    myParams.append('token', token1);	
        let options = new RequestOptions({ headers: myHeaders, params: myParams });
        return this._http.get<Book>(`${this.url}`,myParams);

    }

}
