import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Token } from '../models/user';
import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
})

export class LoginService {
    constructor(private _http: HttpClient) { }
  
 private url = 'http://localhost:3000/login/'; 

  //  private url = 'https://reqres.in/api/login'; 

        getLogin(email: string, password: string): Observable < Token > {
    const data = {
        id: email,
        password: password
    }   
        return this._http.post<Token>(`${this.url}${data.id}`,  data)
                         
}

//  getAllLogin(): Observable < User > {
   
//         return this._http.get<User>(this.url)
                         
// }


}

