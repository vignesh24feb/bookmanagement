import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../service/logon.service';
import { Token } from '../models/user';
import { User } from '../models/user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _logonService: LoginService) { }
  // loginForm: FormGroup;

  ngOnInit() {
  }

  loginForm = new FormGroup({
    // tslint:disable-next-line
    email: new FormControl('', [Validators.required, Validators.minLength(8), Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(5)]),
  });

  private token: Token;
  private user: User;
  private isConnected: boolean = false;

  private error: Error;
  public login() {
    console.log(this.loginForm.value.email);
    this._logonService.getLogin(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(
        (token) => {
          if (this.token) {
            this.token = token;
            this.isConnected = true;
            console.log(this.token);
            //     console.log("returned from the success service")
            sessionStorage.setItem("token", JSON.stringify(this.token));
          }
          else {
            console.log("No token recevied");
          }
        },
        (error) => {
          this.error = error;
          console.log(this.error);
          console.log("returned from the error service")
        },
    )

  }

  public loginAll() {
    console.log(this.loginForm.value.email);
    this._logonService.getAllLogin()
      .subscribe(
        (user) => {
          //      this.user = User;
          console.log(user),
            console.log("returned from the success service")
        },
        (error) => {
          this.error = error;
          console.log(this.error);
          console.log("returned from the error service")
        },
    )

  }
}
