import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {MatFormFieldModule} from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BookComponent } from './book/book.component';
//import { BrowserXhr } from '@angular/http';
//import {CustExtBrowserXhr} from './cust-ext-browser-xhr';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BookComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
   // {provide: BrowserXhr, useClass:CustExtBrowserXhr},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
